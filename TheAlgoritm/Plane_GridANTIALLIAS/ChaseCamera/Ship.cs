#region File Description
//-----------------------------------------------------------------------------
// Ship.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using System;
#endregion

namespace ChaseCameraSample
{
    class Ship
    {
        #region Fields


        private const float MinimumAltitude = 350.0f;

        /// <summary>
        /// A reference to the graphics device used to access the viewport for touch input.
        /// </summary>
        private GraphicsDevice graphicsDevice;

        /// <summary>
        /// Location of ship in world space.
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Location of Planes in world space.
        /// </summary>
        public Vector3 PositionPlanes;

        //public Vector3 PositionSleda;

        // This constant controls how quickly the tank can move forward and backward
        const float TankVelocity = 2;

        // controls how quickly the tank can turn from side to side.
        const float TankTurnSpeed = 0.09f;

        /// <summary>
        /// Direction ship is facing.
        /// </summary>
        public Vector3 Direction;

        //Direction of Sleda :)

       // public Vector3 Direction1;


        /// <summary>
        /// Ship's up vector.
        /// </summary>
        public Vector3 Up;

        //Sleda's UP vector :)


     //   public Vector3 Up1;

        private Vector3 right;
        /// <summary>
        /// Ship's right vector.
        /// </summary>
        
 //Ship's right vector :)

      //  private Vector3 right1;




        public Vector3 Right
        {
            get { return right; }
        }



        /// <summary>
        /// Full speed at which ship can rotate; measured in radians per second.
        /// </summary>
        private const float RotationRate = 1.5f;

        /// <summary> 
        /// Mass of ship.
        /// </summary>
        private const float Mass = 1.0f;
        ///(.)(.);Staroto gaze vleze:Nooooooooooooooooooooooooooooooooooo
        //////nqmq moa pu6a
            ///hAHAHAHAHA
            ///IVANE BRAT , GOLQM SI IDIOT
            ///NE I KAZAH AZ 4E MOJE DA VLEZE
        /// <summary>
        /// Maximum force that can be applied along the ship's direction.
        /// </summary>
        private const float ThrustForce = 24000.0f;

        /// <summary>
        /// Velocity scalar to approximate drag.
        /// </summary>
        private const float DragFactor = 0.97f;

        /// <summary>
        /// Current ship velocity.
        /// </summary>
        public Vector3 Velocity;


        //Taken from the old solution

        public float FacingDirection
        {
            get
            {
                return facingDirection;
            }
        }

        private float facingDirection;
        

        //This is another thing i take from the old solution
        Matrix orientation = Matrix.Identity;



        /// <summary>
        /// Ship world transform matrix.
        /// </summary>
        public Matrix World
        {
            get { return world; }
        }
        private Matrix world;

        #endregion

        #region Initialization

        public Ship(GraphicsDevice device)
        {
            graphicsDevice = device;
            Reset();
        }
        

        /// <summary>
        /// Restore the ship to its original starting state
        /// </summary>
        /// skorost,dvijenie
        public void Reset()
        {
            Position = new Vector3(0, MinimumAltitude, 0);
            PositionPlanes = new Vector3(0, MinimumAltitude, 0);
            Direction = Vector3.Forward;
            Up = Vector3.Up;
            right = Vector3.Right;
            Velocity = Vector3.Zero;
        }

        #endregion

        bool TouchLeft()
        {
            MouseState mouseState = Mouse.GetState();
            return mouseState.LeftButton == ButtonState.Pressed &&
                mouseState.X <= graphicsDevice.Viewport.Width / 3;
        }

        bool TouchRight()
        {
            MouseState mouseState = Mouse.GetState();
            return mouseState.LeftButton == ButtonState.Pressed &&
                mouseState.X >= 2 * graphicsDevice.Viewport.Width / 3;
        }

   //     bool TouchDown()
   //     {
   //         MouseState mouseState = Mouse.GetState();
   //         return mouseState.LeftButton == ButtonState.Pressed &&
   //             mouseState.Y <= graphicsDevice.Viewport.Height / 3;
   //     }

   //     bool TouchUp()
   //     {
   //         MouseState mouseState = Mouse.GetState();
   //         return mouseState.LeftButton == ButtonState.Pressed &&
   //             mouseState.Y >= 2 * graphicsDevice.Viewport.Height / 3;
   //     }

        /// <summary>
        /// Applies a simple rotation to the ship and animates position based
        /// on simple linear motion physics.
        /// </summary>
        
        #region Update and Draw
                #region
                /// <summary>
                /// This function is called when the game is Updating in response to user input.
                /// It'll move the tank around the heightmap, and update all of the tank's 
                /// necessary state.
                /// </summary>
        public void HandleInput(GamePadState currentGamePadState,
                                 KeyboardState currentKeyboardState,
                                 ChaseCamera heightMapInfo)
        {
            // First, we want to check to see if the tank should turn. turnAmount will 
            // be an accumulation of all the different possible inputs.
            float turnAmount = -currentGamePadState.ThumbSticks.Left.X;
            if (currentKeyboardState.IsKeyDown(Keys.A) ||
                 currentKeyboardState.IsKeyDown(Keys.Left) ||
                 currentGamePadState.DPad.Left == ButtonState.Pressed)
            {
                turnAmount += 1;
            }

            if (currentKeyboardState.IsKeyDown(Keys.D) ||
                 currentKeyboardState.IsKeyDown(Keys.Right) ||
                 currentGamePadState.DPad.Right == ButtonState.Pressed)
            {
                turnAmount -= 1;
            }

            // clamp the turn amount between -1 and 1, and then use the finished
            // value to turn the tank.
            turnAmount = MathHelper.Clamp(turnAmount, -1, 1);
            facingDirection += turnAmount * TankTurnSpeed;
                #endregion
            // Next, we want to move the tank forward or back. to do this, 
            // we'll create a Vector3 and modify use the user's input to modify the Z
            // component, which corresponds to the forward direction.
            Vector3 movement = Vector3.Zero;
            movement.Z = -currentGamePadState.ThumbSticks.Left.Y;

            if (currentKeyboardState.IsKeyDown(Keys.W) ||
                 currentKeyboardState.IsKeyDown(Keys.Up) ||
                 currentGamePadState.DPad.Up == ButtonState.Pressed)
            {
                movement.Z = -10;
            }

            if (currentKeyboardState.IsKeyDown(Keys.S) ||
                 currentKeyboardState.IsKeyDown(Keys.Down) ||
                 currentGamePadState.DPad.Down == ButtonState.Pressed)
            {
                movement.Z = 10;
            }

            // next, we'll create a rotation matrix from the direction the tank is 
            // facing, and use it to transform the vector.
            //rotation when moving 
            //When standing on the end and changed by Z than it rotates arond itself;
            //When changed by Y it is just normal
            //And when changed by X it rotates by the way I wand
            orientation = Matrix.CreateRotationX(FacingDirection);
            Vector3 velocity = Vector3.Transform(movement, orientation);
            velocity *= TankVelocity;
        }
#endregion


        public void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            MouseState mouseState = Mouse.GetState();

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;


            // Determine rotation amount from input
            Vector3 rotationAmount = Vector3.Zero;
            //rotationAmount.Z = ThumbSticks.Left.Y;
            //Vector2 rotationAmount = -gamePadState.ThumbSticks.Left;
            if (keyboardState.IsKeyDown(Keys.Left) || TouchLeft())
                rotationAmount.X = 1.0f;
            if (keyboardState.IsKeyDown(Keys.Right) || TouchRight())
                rotationAmount.X = -1.0f;
      //      if (keyboardState.IsKeyDown(Keys.Up) || TouchUp())
      //          rotationAmount.Y = -1.0f;
      //     if (keyboardState.IsKeyDown(Keys.Down) || TouchDown())
      //          rotationAmount.Y = 1.0f;

            
            // Scale rotation amount to radians per second
            rotationAmount = rotationAmount * RotationRate * elapsed;

            // Correct the X axis steering when the ship is upside down
            //Old one
     //       if (Up.Y < 0)
     //           rotationAmount.X = -rotationAmount.X;








           // Matrix wheelRollMatrix = Matrix.Identity;
          //  wheelRollMatrix *= Matrix.CreateRotationX(Direction);
            


            //okolo 60000
            //0:12000 za z vectoryt
            orientation = Matrix.CreateRotationX(FacingDirection);
            
           // if (Position.X < PositionPlanes.X - 60000)
           // { rotationAmount.Y = -1.0f; }
           // if (Position.X > PositionPlanes.X + 60000)
           // { rotationAmount.Y = -1.0f; }
           // if (Position.Y < PositionPlanes.Y - 60000)
           // { rotationAmount.Y = -1.0f; }
           // if (Position.Y > PositionPlanes.Y + 60000)
           // { rotationAmount.Y = -1.0f; }
           // if (Position.Z < PositionPlanes.Z - 60000)
           // { rotationAmount.Y = -1.0f; }
           // if (Position.Z > PositionPlanes.Z + 60000)
           // { rotationAmount.Y = -1.0f; }

            //Smeni if-a ot .up na .space

            if (Position.X < PositionPlanes.X - 60000)
            { 
                if (keyboardState.IsKeyDown(Keys.Up) )
                rotationAmount.Y = -1.0f;
           if (keyboardState.IsKeyDown(Keys.Space) )
                rotationAmount.Y = 5.0f; 
            }
             if (Position.X > PositionPlanes.X + 60000)
            {
                if (keyboardState.IsKeyDown(Keys.Up))
                    rotationAmount.Y = -1.0f;
                if (keyboardState.IsKeyDown(Keys.Space))
                    rotationAmount.Y = 5.0f;
            }
            if (Position.Y < PositionPlanes.Y - 60000)
            {
                if (keyboardState.IsKeyDown(Keys.Up))
                    rotationAmount.Y = -1.0f;
                if (keyboardState.IsKeyDown(Keys.Down))
                    rotationAmount.Y = 1.0f;
            }
            if (Position.Y > PositionPlanes.Y + 60000)
            {
                if (keyboardState.IsKeyDown(Keys.Up))
                    rotationAmount.Y = -1.0f;
                if (keyboardState.IsKeyDown(Keys.Down))
                    rotationAmount.Y = 1.0f;
            }
            if (Position.Z < PositionPlanes.Z - 60000)
            { rotationAmount.Y = -1.0f; }
            if (Position.Z > PositionPlanes.Z + 60000)
            { rotationAmount.Y = -1.0f; }






            //rotation when moving 
            //When standing on the end and changed by Z than it rotates arond itself;
            //When changed by Y it is just normal
            //And when changed by X it rotates by the way I wand
            




            // Create rotation matrix from rotation amount
            Matrix rotationMatrix =
                Matrix.CreateFromAxisAngle(Right, rotationAmount.Y) *
                Matrix.CreateRotationY(rotationAmount.X);
                Matrix.CreateRotationZ(rotationAmount.X);

            // Rotate orientation vectors
            Direction = Vector3.TransformNormal(Direction, rotationMatrix);
            Up = Vector3.TransformNormal(Up, rotationMatrix);




            















            // Re-normalize orientation vectors
            // Without this, the matrix transformations may introduce small rounding
            // errors which add up over time and could destabilize the ship.
            Direction.Normalize();
            Up.Normalize();











            // Re-calculate Right
            right = Vector3.Cross(Direction, Up);

            // The same instability may cause the 3 orientation vectors may
            // also diverge. Either the Up or Direction vector needs to be
            // re-computed with a cross product to ensure orthagonality
            Up = Vector3.Cross(Right, Direction);


            // Determine thrust amount from input
            float thrustAmount = gamePadState.Triggers.Right;
            if (keyboardState.IsKeyDown(Keys.Space) || mouseState.LeftButton == ButtonState.Pressed)
                thrustAmount = 1.0f;

            // Calculate force from thrust amount
            Vector3 force = Direction * thrustAmount * ThrustForce;


            // Apply acceleration
            Vector3 acceleration = force / Mass;
            Velocity += acceleration * elapsed;

            // Apply psuedo drag
            Velocity *= DragFactor;

            // Apply velocity
            Position += Velocity * elapsed;


            // Prevent ship from flying under the ground
            //idea
            //            if (Position.X < 0)
            //                rotationAmount.Y = -rotationAmount.X;

//if (Position.X >= -10000 && Position.X <= 10000 
//    && Position.Y >= -10000 && Position.Y <= 10000 
//    && Position.Z >= -10000 && Position.Z <= 10000) 
//{ rotationAmount.Y = -100000.0f; }

//idea
//          if (Position.X >= -100.0f && Position.X <= 100.0f
//  && Position.Y >= -100 && Position.Y <= 100
//  && Position.Z >= -100 && Position.Z <= 100)
//           rotationAmount.Y = -100.0f; 
// Correct the X axis steering when the ship is upside down
//         if (Up.Y < 0)
//             rotationAmount.X = -rotationAmount.X;
//        if (Up.X < 0)
//             rotationAmount.X = -rotationAmount.Y;
             //        if (Position.X < 0)
             //            rotationAmount.Y = -rotationAmount.X;

 //           
 //           Position.Z = Math.Max(Position.Z, MinimumAltitude);
 //           Position.X = Math.Max(Position.X, MinimumAltitude);
         //   float X1 = new float (Vector3.UnitX;)
         //  float X2 = new float (Vector3.UnitX);
         //   float Z1 = new float (Vector3.UnitZ;)
           // Position.X = 2000;
           // Position.Z = 2000;
           // Position.Y = 2000;
            
        //    float Position.X1, Position.Y1, Position.Z1;
       //     Position.X1 = (float)Position.X ;
      //      Position.Y1 = (float)Position.Z ;
     //       Position.Z1 = (float)Position.Y ;

     //       Position.Y1 = Math.Max(Position.Y1, MinimumAltitude);
      //      Position.Z1 = Math.Max(Position.Z1, MinimumAltitude);
       //     Position.X1 = Math.Max(Position.X1, MinimumAltitude);
           // public static bool ToBoolean(
           // bool value
           // );
    //        Position.

            
      //      for(Vector3(100,100,100) = Vector3(100,100,100))
      //  {
      //      Position = new Vector3(10, MinimumAltitude, 10);
      //      Direction = Vector3.Forward;
      //      Up = Vector3.Up;
      //      right = Vector3.Right;
      //      Velocity = Vector3.Zero;
      //  }


           // if(position    Position.Y = Posion.X


            // now we need to roll the tank's wheels "forward." to do this, we'll
            // calculate how far they have rolled, and from there calculate how much
            // they must have rotated.
            //  float distanceMoved = Vector3.Distance(Position, newPosition);
            //  float theta = distanceMoved / TankWheelRadius;
       //     int rollDirection = thrustAmount > 0 ? 1 : -1;
       //     Matrix wheelRollMatrix = Matrix.Identity;
       //     wheelRollMatrix *= Matrix.CreateRotationX(rollDirection);

            // once we've finished all computations, we can set our position to the
            // new position that we calculated.
            //position = newPosition;














            
            
            
            
            //Convert.ChangeType(
          //  Convert.ChangeType(10,float,Boolean)
         //   Ship..ToString 
            //if(Position.X = 100.0f && Position.Y=100.0f && Position.Z=0.0f) 
            //float NearPlaneDistance = Position * Direction;

            //int array

            //double Position1 = 2006;
            //float Position = 0;
            //Position = (float)Position1;

            
           // string positions = Convert.ToBoolean("true");
           // do
           // {
           //     rotationAmount.Y = -1.0f;
           // }
           // while (positions = "0" );


//Math.Acos(90)
//&& Position.Y=0 && Position.Z=0




            // Reconstruct the ship's world matrix
            world = Matrix.Identity;
            world.Forward = Direction;
            world.Up = Up;
            world.Right = right;
            world.Translation = Position;
        }
    }
    
}
