﻿#region Using Statements

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics.PackedVector;
#endregion

namespace ChaseCameraSample
{
       class Sleda
        {
            #region Fields


            private const float MinimumAltitude1 = 350.0f;

            /// <summary>
            /// A reference to the graphics device used to access the viewport for touch input.
            /// </summary>
            private GraphicsDevice graphicsDevice1;

            /// <summary>
            /// Location of ship in world space.
            /// </summary>
            public Vector3 Position1;


            public Vector3 PositionPlanes1;


            //public Vector3 PositionSleda;

            // This constant controls how quickly the tank can move forward and backward
            const float TankVelocity1 = 2;

            // controls how quickly the tank can turn from side to side.
            const float TankTurnSpeed1 = 0.09f;

            /// <summary>
            /// Direction ship is facing.
            /// </summary>
            public Vector3 Direction1;

            //Direction of Sleda :)

            // public Vector3 Direction1;


            /// <summary>
            /// Ship's up vector.
            /// </summary>
            public Vector3 Up1;

            //Sleda's UP vector :)


            //   public Vector3 Up1;

            private Vector3 right1;
            /// <summary>
            /// Ship's right vector.
            /// </summary>

            //Ship's right vector :)

            //  private Vector3 right1;




            public Vector3 Right1
            {
                get { return right1; }
            }
            /// <summary>
            /// Full speed at which ship can rotate; measured in radians per second.
            /// </summary>
            private const float RotationRate1 = 1.5f;

            /// <summary> 
            /// Mass of ship.
            /// </summary>
            private const float Mass1 = 1.0f;
            ///(.)(.);Staroto gaze vleze:Nooooooooooooooooooooooooooooooooooo
            //////nqmq moa pu6a
            ///hAHAHAHAHA
            ///IVANE BRAT , GOLQM SI IDIOT
            ///NE I KAZAH AZ 4E MOJE DA VLEZE
            /// <summary>
            /// Maximum force that can be applied along the ship's direction.
            /// </summary>
            private const float ThrustForce1 = 24000.0f;

            /// <summary>
            /// Velocity scalar to approximate drag.
            /// </summary>
            private const float DragFactor1 = 0.97f;

            /// <summary>
            /// Current ship velocity.
            /// </summary>
            public Vector3 Velocity1;


            //Taken from the old solution

            public float FacingDirection1
            {
                get
                {
                    return facingDirection1;
                }
            }

            private float facingDirection1;

           //thrustAmount1 

            public float ThrustAmount1
            {
                get
                {
                    return thrustAmount1;
                }
            }

            private float thrustAmount1;




            //This is another thing i take from the old solution
            Matrix orientation1 = Matrix.Identity;



            /// <summary>
            /// Ship world transform matrix.
            /// </summary>
            public Matrix World1
            {
                get { return world1; }
            }
            public Matrix world1;

            #endregion

            #region Initialization1

            public Sleda(GraphicsDevice device)
            {
                graphicsDevice1 = device;
                Reset();
            }

            /// <summary>
            /// Restore the ship to its original starting state
            /// </summary>
            /// skorost,dvijenie
            public void Reset()
            {
                Position1 = new Vector3(0, MinimumAltitude1, 0);
                PositionPlanes1 = new Vector3(0, MinimumAltitude1, 0);
                Direction1 = Vector3.Forward;
                Up1 = Vector3.Up;
                right1 = Vector3.Right;
                Velocity1 = Vector3.Zero;
            }

            #endregion

            bool TouchLeft1()
            {
                MouseState mouseState = Mouse.GetState();
                return mouseState.LeftButton == ButtonState.Pressed &&
                    mouseState.X <= graphicsDevice1.Viewport.Width / 3;
            }

            bool TouchRight1()
            {
                MouseState mouseState1 = Mouse.GetState();
                return mouseState1.LeftButton == ButtonState.Pressed &&
                    mouseState1.X >= 2 * graphicsDevice1.Viewport.Width / 3;
            }

            //     bool TouchDown()
            //     {
            //         MouseState mouseState = Mouse.GetState();
            //         return mouseState.LeftButton == ButtonState.Pressed &&
            //             mouseState.Y <= graphicsDevice.Viewport.Height / 3;
            //     }

            //     bool TouchUp()
            //     {
            //         MouseState mouseState = Mouse.GetState();
            //         return mouseState.LeftButton == ButtonState.Pressed &&
            //             mouseState.Y >= 2 * graphicsDevice.Viewport.Height / 3;
            //     }

            /// <summary>
            /// Applies a simple rotation to the ship and animates position based
            /// on simple linear motion physics.
            /// </summary>

            #region Update and Draw
            #region
            /// <summary>
            /// This function is called when the game is Updating in response to user input.
            /// It'll move the tank around the heightmap, and update all of the tank's 
            /// necessary state.
            /// </summary>
            public void HandleInput(GamePadState currentGamePadState,
                                     KeyboardState currentKeyboardState,
                                     ChaseCamera heightMapInfo)
            {
                // First, we want to check to see if the tank should turn. turnAmount will 
                // be an accumulation of all the different possible inputs.
                float turnAmount1 = -currentGamePadState.ThumbSticks.Left.X;
                if (currentKeyboardState.IsKeyDown(Keys.A) ||
                     currentKeyboardState.IsKeyDown(Keys.Left) ||
                     currentGamePadState.DPad.Left == ButtonState.Pressed)
                {
                    turnAmount1 += 1;
                }

                if (currentKeyboardState.IsKeyDown(Keys.D) ||
                     currentKeyboardState.IsKeyDown(Keys.Right) ||
                     currentGamePadState.DPad.Right == ButtonState.Pressed)
                {
                    turnAmount1 -= 1;
                }

                // clamp the turn amount between -1 and 1, and then use the finished
                // value to turn the tank.
                turnAmount1 = MathHelper.Clamp(turnAmount1, -1, 1);
                facingDirection1 += turnAmount1 * TankTurnSpeed1;
            #endregion
                // Next, we want to move the tank forward or back. to do this, 
                // we'll create a Vector3 and modify use the user's input to modify the Z
                // component, which corresponds to the forward direction.
                Vector3 movement1 = Vector3.Zero;
                movement1.Z = -currentGamePadState.ThumbSticks.Left.Y;

                if (currentKeyboardState.IsKeyDown(Keys.W) ||
                     currentKeyboardState.IsKeyDown(Keys.Up) ||
                     currentGamePadState.DPad.Up == ButtonState.Pressed)
                {
                    movement1.Z = -10;
                }

                if (currentKeyboardState.IsKeyDown(Keys.S) ||
                     currentKeyboardState.IsKeyDown(Keys.Down) ||
                     currentGamePadState.DPad.Down == ButtonState.Pressed)
                {
                    movement1.Z = 10;
                }

                // next, we'll create a rotation matrix from the direction the tank is 
                // facing, and use it to transform the vector.
                //rotation when moving 
                //When standing on the end and changed by Z than it rotates arond itself;
                //When changed by Y it is just normal
                //And when changed by X it rotates by the way I wand
                orientation1 = Matrix.CreateRotationX(FacingDirection1);
                Vector3 velocity1 = Vector3.Transform(movement1, orientation1);
                velocity1 *= TankVelocity1;
            }
            #endregion


            public void Update(GameTime gameTime)
            {
                KeyboardState keyboardState = Keyboard.GetState();
                GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
                MouseState mouseState = Mouse.GetState();

                float elapsed1 = (float)gameTime.ElapsedGameTime.TotalSeconds;


                // Determine rotation amount from input
                Vector3 rotationAmount1 = Vector3.Zero;
                //rotationAmount.Z = ThumbSticks.Left.Y;
                //Vector2 rotationAmount = -gamePadState.ThumbSticks.Left;
                if (keyboardState.IsKeyDown(Keys.Left) || TouchLeft1())
                    rotationAmount1.X = 1.0f;
                if (keyboardState.IsKeyDown(Keys.Right) || TouchRight1())
                    rotationAmount1.X = -1.0f;
                //      if (keyboardState.IsKeyDown(Keys.Up) || TouchUp())
                //          rotationAmount.Y = -1.0f;
                //     if (keyboardState.IsKeyDown(Keys.Down) || TouchDown())
                //          rotationAmount.Y = 1.0f;


                // Scale rotation amount to radians per second
                rotationAmount1 = rotationAmount1 * RotationRate1 * elapsed1;

                // Correct the X axis steering when the ship is upside down
                //Old one
                //       if (Up.Y < 0)
                //           rotationAmount.X = -rotationAmount.X;








                // Matrix wheelRollMatrix = Matrix.Identity;
                //  wheelRollMatrix *= Matrix.CreateRotationX(Direction);



                //okolo 60000
                //0:12000 za z vectoryt
                orientation1 = Matrix.CreateRotationX(FacingDirection1);

                // if (Position.X < PositionPlanes.X - 60000)
                // { rotationAmount.Y = -1.0f; }
                // if (Position.X > PositionPlanes.X + 60000)
                // { rotationAmount.Y = -1.0f; }
                // if (Position.Y < PositionPlanes.Y - 60000)
                // { rotationAmount.Y = -1.0f; }
                // if (Position.Y > PositionPlanes.Y + 60000)
                // { rotationAmount.Y = -1.0f; }
                // if (Position.Z < PositionPlanes.Z - 60000)
                // { rotationAmount.Y = -1.0f; }
                // if (Position.Z > PositionPlanes.Z + 60000)
                // { rotationAmount.Y = -1.0f; }

                //Smeni if-a ot .up na .space

                if (Position1.X < PositionPlanes1.X - 60000)
                {
                    if (keyboardState.IsKeyDown(Keys.Up))
                        rotationAmount1.Y = -1.0f;
                    if (keyboardState.IsKeyDown(Keys.Space))
                        rotationAmount1.Y = 5.0f;
                }
                if (Position1.X > PositionPlanes1.X + 60000)
                {
                    if (keyboardState.IsKeyDown(Keys.Up))
                        rotationAmount1.Y = -1.0f;
                    if (keyboardState.IsKeyDown(Keys.Space))
                        rotationAmount1.Y = 5.0f;
                }
                if (Position1.Y < PositionPlanes1.Y - 60000)
                {
                    if (keyboardState.IsKeyDown(Keys.Up))
                        rotationAmount1.Y = -1.0f;
                    if (keyboardState.IsKeyDown(Keys.Down))
                        rotationAmount1.Y = 1.0f;
                }
                if (Position1.Y > PositionPlanes1.Y + 60000)
                {
                    if (keyboardState.IsKeyDown(Keys.Up))
                        rotationAmount1.Y = -1.0f;
                    if (keyboardState.IsKeyDown(Keys.Down))
                        rotationAmount1.Y = 1.0f;
                }
                if (Position1.Z < PositionPlanes1.Z - 60000)
                { rotationAmount1.Y = -1.0f; }
                if (Position1.Z > PositionPlanes1.Z + 60000)
                { rotationAmount1.Y = -1.0f; }






                //rotation when moving 
                //When standing on the end and changed by Z than it rotates arond itself;
                //When changed by Y it is just normal
                //And when changed by X it rotates by the way I wand





                // Create rotation matrix from rotation amount
                Matrix rotationMatrix =
                    Matrix.CreateFromAxisAngle(Right1, rotationAmount1.Y) *
                    Matrix.CreateRotationY(rotationAmount1.X);
                Matrix.CreateRotationZ(rotationAmount1.X);

                // Rotate orientation vectors
                Direction1 = Vector3.TransformNormal(Direction1, rotationMatrix);
                Up1 = Vector3.TransformNormal(Up1, rotationMatrix);






#region snake
    //            {
   //                 {base.Paint += new PaintEventHandler(this.Sleda_Paint);
  //              base.KeyDown += new KeyEventHandler(this.SnakeCSharpWindow_KeyDown);
   //             this.t.Tick += new EventHandler(this.t_Tick);
   //             this.t.Start();
    //            this.snake.AddFirst(new Point(10, 10));}
//
    //    private void Sleda_KeyDown ( object sender, KeyEventArgs e )
   //     {
   ////         if ( e.KeyCode == Keys.Left )
   //         {
  //              this.dir = new Point ( -10, 0 ) ;
  //          }
  //          if ( e.KeyCode == Keys.Right )
 //           {
 //               this.dir = new Point ( 10, 0 ) ;
  //          }
  //          if ( e.KeyCode == Keys.Up )
  //          {
  //              this.dir = new Point ( 0, -10 ) ;
//            }
//            if ( e.KeyCode == Keys.Down )
//            {
//                this.dir = new Point ( 0, 10 ) ;
//            }
 //       }
//
 //       private void Snake_Paint ( object sender, PaintEventArgs e )
 //       {
 //           int num = 0 ;
 //           foreach ( Point point in this.Snake_Paint )
//            {
//                e.Graphics.FillRectangle ( Brushes.Red, point.X, point.Y, 10, 10 ) ;
//                if ( ( ( num != 0 ) && ( this.snake.First.Value.X == point.X ) ) &&
//                     ( this.Snake_Paint.First.Value.Y == point.Y ) )
///                {
 //                   this.t.Stop ( ) ;
 //               }
 //               num++ ;
 //           }
 //       }
//
 //       private void t_Tick ( object sender, EventArgs e )
 //       {
 //           this.snake.AddFirst (
 //               new Point ( this.snake.First.Value.X + this.dir.X,
 //                          this.snake.First.Value.Y + this.dir.Y ) ) ;
 //           
 //           this.Refresh ( ) ;
 //       }
 //}
            #endregion









                // Re-normalize orientation vectors
                // Without this, the matrix transformations may introduce small rounding
                // errors which add up over time and could destabilize the ship.
                Direction1.Normalize();
                Up1.Normalize();











                // Re-calculate Right
                right1 = Vector3.Cross(Direction1, Up1);

                // The same instability may cause the 3 orientation vectors may
                // also diverge. Either the Up or Direction vector needs to be
                // re-computed with a cross product to ensure orthagonality
                Up1 = Vector3.Cross(Right1, Direction1);


                // Determine thrust amount from input
                 float thrustAmount1 = gamePadState.Triggers.Right;
                if (keyboardState.IsKeyDown(Keys.Space) || mouseState.LeftButton == ButtonState.Pressed)
                    thrustAmount1 = 1.0f;

                // Calculate force from thrust amount
                Vector3 force1 = Direction1 * thrustAmount1 * ThrustForce1;


                // Apply acceleration
                Vector3 acceleration1 = force1 / Mass1;
                Velocity1 += acceleration1 * elapsed1;

                // Apply psuedo drag
                Velocity1 *= DragFactor1;

                // Apply velocity
                Position1 += Velocity1 * elapsed1;


                // Prevent ship from flying under the ground
                //idea
                //            if (Position.X < 0)
                //                rotationAmount.Y = -rotationAmount.X;

                //if (Position.X >= -10000 && Position.X <= 10000 
                //    && Position.Y >= -10000 && Position.Y <= 10000 
                //    && Position.Z >= -10000 && Position.Z <= 10000) 
                //{ rotationAmount.Y = -100000.0f; }

                //idea
                //          if (Position.X >= -100.0f && Position.X <= 100.0f
                //  && Position.Y >= -100 && Position.Y <= 100
                //  && Position.Z >= -100 && Position.Z <= 100)
                //           rotationAmount.Y = -100.0f; 
                // Correct the X axis steering when the ship is upside down
                //         if (Up.Y < 0)
                //             rotationAmount.X = -rotationAmount.X;
                //        if (Up.X < 0)
                //             rotationAmount.X = -rotationAmount.Y;
                //        if (Position.X < 0)
                //            rotationAmount.Y = -rotationAmount.X;

                //           
                //           Position.Z = Math.Max(Position.Z, MinimumAltitude);
                //           Position.X = Math.Max(Position.X, MinimumAltitude);
                //   float X1 = new float (Vector3.UnitX;)
                //  float X2 = new float (Vector3.UnitX);
                //   float Z1 = new float (Vector3.UnitZ;)
                // Position.X = 2000;
                // Position.Z = 2000;
                // Position.Y = 2000;

                //    float Position.X1, Position.Y1, Position.Z1;
                //     Position.X1 = (float)Position.X ;
                //      Position.Y1 = (float)Position.Z ;
                //       Position.Z1 = (float)Position.Y ;

                //       Position.Y1 = Math.Max(Position.Y1, MinimumAltitude);
                //      Position.Z1 = Math.Max(Position.Z1, MinimumAltitude);
                //     Position.X1 = Math.Max(Position.X1, MinimumAltitude);
                // public static bool ToBoolean(
                // bool value
                // );
                //        Position.


                //      for(Vector3(100,100,100) = Vector3(100,100,100))
                //  {
                //      Position = new Vector3(10, MinimumAltitude, 10);
                //      Direction = Vector3.Forward;
                //      Up = Vector3.Up;
                //      right = Vector3.Right;
                //      Velocity = Vector3.Zero;
                //  }


                // if(position    Position.Y = Posion.X


                // now we need to roll the tank's wheels "forward." to do this, we'll
                // calculate how far they have rolled, and from there calculate how much
                // they must have rotated.
                //  float distanceMoved = Vector3.Distance(Position, newPosition);
                //  float theta = distanceMoved / TankWheelRadius;
                //     int rollDirection = thrustAmount > 0 ? 1 : -1;
                //     Matrix wheelRollMatrix = Matrix.Identity;
                //     wheelRollMatrix *= Matrix.CreateRotationX(rollDirection);

                // once we've finished all computations, we can set our position to the
                // new position that we calculated.
                //position = newPosition;




    //            //   float thrustAmount1;
     //           if (keyboardState.IsKeyDown(Keys.Z))
      //          {
      //              do
      //              {
       //                 DrawModel(sledaModel, sleda.World1);
      //                  //    thrustAmount1 = 2.0f;
      //              }
      //              while (sleda.Position1 != ship.Position);
     //           }












                //Convert.ChangeType(
                //  Convert.ChangeType(10,float,Boolean)
                //   Ship..ToString 
                //if(Position.X = 100.0f && Position.Y=100.0f && Position.Z=0.0f) 
                //float NearPlaneDistance = Position * Direction;

                //int array

                //double Position1 = 2006;
                //float Position = 0;
                //Position = (float)Position1;


                // string positions = Convert.ToBoolean("true");
                // do
                // {
                //     rotationAmount.Y = -1.0f;
                // }
                // while (positions = "0" );


                //Math.Acos(90)
                //&& Position.Y=0 && Position.Z=0




                // Reconstruct the ship's world matrix
                world1 = Matrix.Identity;
                world1.Forward = Direction1;
                world1.Up = Up1;
                world1.Right = right1;
                world1.Translation = Position1;
            }
        }
}
