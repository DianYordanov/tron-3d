using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BoundingBox40
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Base3DCamera camera;

        Base3DObject obj1, obj2;

        SpriteFont font;

        Vector3 lightPos = new Vector3(100, 100, 50);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            camera = new Base3DCamera(this, .1f, 20000);
            camera.Position = new Vector3(0, 0, 0);
            Components.Add(camera);
            Services.AddService(typeof(Base3DCamera), camera);

            obj1 = new Base3DObject(this, "Models/Cube");
            obj1.Position = new Vector3(0, 0, -10);
            obj1.LightPosition = lightPos;
            Components.Add(obj1);

            obj2 = new Base3DObject(this, "Models/Cube");
            obj2.Position = new Vector3(-5, 0, -10);
            obj2.LightPosition = lightPos;
            Components.Add(obj2);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font = Content.Load<SpriteFont>("Fonts/font");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) || GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // Camera controls..
            float speedTran = .1f;
            float speedRot = .01f;
            if (Keyboard.GetState().IsKeyDown(Keys.W))
                camera.Translate(Vector3.Forward * speedTran);
            if (Keyboard.GetState().IsKeyDown(Keys.S))
                camera.Translate(Vector3.Backward * speedTran);
            if (Keyboard.GetState().IsKeyDown(Keys.A))
                camera.Translate(Vector3.Left * speedTran);
            if (Keyboard.GetState().IsKeyDown(Keys.D))
                camera.Translate(Vector3.Right * speedTran);

            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                camera.Rotate(Vector3.Up, speedRot);
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                camera.Rotate(Vector3.Up, -speedRot);
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
                camera.Rotate(Vector3.Right, speedRot);
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
                camera.Rotate(Vector3.Right, -speedRot);

            // Move box 1
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad4))
                obj1.Translate(Vector3.Left * .1f);
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad6))
                obj1.Translate(Vector3.Right * .1f);

            if (Keyboard.GetState().IsKeyDown(Keys.NumPad8))
                obj1.Translate(Vector3.Forward * .1f);
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad2))
                obj1.Translate(Vector3.Backward * .1f);

            // Collisonon check.
            obj1.Collided(obj2);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            spriteBatch.DrawString(font, "AWSD - Translate camera", new Vector2(5, 0), Color.Red);
            spriteBatch.DrawString(font, "NumPad Arrows - Translate Cube1", new Vector2(5, font.LineSpacing), Color.Red);
            spriteBatch.End();

        }
        

        
    }
}
