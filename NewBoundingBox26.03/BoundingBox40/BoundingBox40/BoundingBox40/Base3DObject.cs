﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using BoundingBox40.Utilities;

namespace BoundingBox40
{
    public class Base3DObject : DrawableGameComponent
    {
        string modelName;
        
        public Base3DCamera camera
        {
            get { return (Base3DCamera)Game.Services.GetService(typeof(Base3DCamera)); }
        }

        /// <summary>
        /// Position
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Scale
        /// </summary>
        public Vector3 Scale;

        /// <summary>
        /// Rotation
        /// </summary>
        public Quaternion Rotation;

        Model mesh;

        /// <summary>
        /// World
        /// </summary>
        public Matrix World;

        public BasicEffect Effect;

        Matrix[] transforms;
        Matrix meshWorld;
        Matrix meshWVP;

        public Vector3 LightPosition = new Vector3(10, 10, 0);
        public Color AmbientColor = Color.CornflowerBlue;
        public Color DiffuseColor = Color.SteelBlue;

        BoundingBox volume;
        public BoundingBox bounds;
        public Color boxCol = Color.Black;

        VertexPositionColor[] points;
        short[] index;
        BasicEffect lineShader;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="game"></param>
        /// <param name="modelAssetName"></param>
        /// <param name="shaderAssetName"></param>
        public Base3DObject(Game game, string modelAssetName) : base(game)
        {
            Position = Vector3.Zero;
            Scale = Vector3.One;
            Rotation = Quaternion.Identity;
            modelName = modelAssetName;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            World = Matrix.CreateScale(Scale) *
                      Matrix.CreateFromQuaternion(Rotation) *
                      Matrix.CreateTranslation(Position);

            if (mesh == null && modelName != string.Empty)
            {
                mesh = Game.Content.Load<Model>(modelName);

                transforms = new Matrix[mesh.Bones.Count];
                mesh.CopyAbsoluteBoneTransformsTo(transforms);

                Dictionary<string, object> data = (Dictionary<string, object>)mesh.Tag;

                volume = ((List<BoundingBox>)data["BoundingBoxs"])[0];
            }

            if (Effect == null)
                Effect = new BasicEffect(Game.GraphicsDevice);

            bounds = new BoundingBox(Vector3.Transform(volume.Min, World), Vector3.Transform(volume.Max, World));
        }


        /// <summary>
        /// Method to translate object
        /// </summary>
        /// <param name="distance"></param>
        public void Translate(Vector3 distance)
        {
            Position += GameComponentHelper.Translate3D(distance, Rotation);
        }
        
        /// <summary>
        /// Method to rotate object
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="angle"></param>
        public void Rotate(Vector3 axis, float angle)
        {
            GameComponentHelper.Rotate(axis, angle, ref Rotation);
        }


        public bool Collided(Base3DObject bob)
        {
            bool retVal = false;

            bool CollisionChecker = true;

            if (bounds.Intersects(bob.bounds))
            {
                this.boxCol = Color.Pink;
                bob.boxCol = Color.Pink;
                this.DiffuseColor = Color.DarkRed;
                bob.DiffuseColor = Color.DarkRed;
                retVal = true;
                CollisionChecker = false;
            }
            //else
            if(CollisionChecker==true)
            {
                
                this.boxCol = Color.Black;
                bob.boxCol = Color.Black;
             //   this.DiffuseColor = Color.SteelBlue;
             //   bob.DiffuseColor = Color.SteelBlue;
            }

         //   return CollisionChecker;


            return retVal;
        }

        protected void DrawBox(BoundingBox box)
        {
            BuildBox(box, boxCol);
            DrawLines(12);
        }
        protected void BuildBox(BoundingBox box, Color lineColor)
        {
            points = new VertexPositionColor[8];

            Vector3[] corners = box.GetCorners();

            points[0] = new VertexPositionColor(corners[1], lineColor); // Front Top Right
            points[1] = new VertexPositionColor(corners[0], lineColor); // Front Top Left
            points[2] = new VertexPositionColor(corners[2], lineColor); // Front Bottom Right
            points[3] = new VertexPositionColor(corners[3], lineColor); // Front Bottom Left
            points[4] = new VertexPositionColor(corners[5], lineColor); // Back Top Right
            points[5] = new VertexPositionColor(corners[4], lineColor); // Back Top Left
            points[6] = new VertexPositionColor(corners[6], lineColor); // Back Bottom Right
            points[7] = new VertexPositionColor(corners[7], lineColor); // Bakc Bottom Left

            index = new short[] {
	            0, 1, 0, 2, 1, 3, 2, 3,
	            4, 5, 4, 6, 5, 7, 6, 7,
	            0, 4, 1, 5, 2, 6, 3, 7
                };
        }
        protected void DrawLines(int primativeCount)
        {
            if (lineShader == null)
                lineShader = new BasicEffect(GraphicsDevice);

            lineShader.World = Matrix.Identity;
            lineShader.View = camera.View;
            lineShader.Projection = camera.Projection;
            lineShader.VertexColorEnabled = true;

            lineShader.CurrentTechnique.Passes[0].Apply();
            GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, points, 0, points.Length, index, 0, primativeCount);
        }

        public virtual void Draw(GameTime gameTime, BasicEffect effect)
        {
            Game.GraphicsDevice.BlendState = BlendState.Opaque;
            Game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            Game.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            Game.GraphicsDevice.SamplerStates[0] = SamplerState.PointWrap;
            
            effect.AmbientLightColor = AmbientColor.ToVector3();
            effect.DiffuseColor = DiffuseColor.ToVector3();
            effect.DirectionalLight0.Direction = Vector3.Normalize(Position - LightPosition);
            effect.LightingEnabled = true;
            effect.Projection = camera.Projection;
            effect.PreferPerPixelLighting = true;
            effect.View = camera.View;

            foreach (ModelMesh meshM in mesh.Meshes)
            {
                // Do the world stuff. 
                // Scale * transform * pos * rotation
                meshWorld = transforms[meshM.ParentBone.Index] * World;
                meshWVP = meshWorld * camera.View * camera.Projection;

                effect.World = meshWorld;

                effect.CurrentTechnique.Passes[0].Apply();

                foreach (ModelMeshPart meshPart in meshM.MeshParts)
                {
                    Game.GraphicsDevice.SetVertexBuffer(meshPart.VertexBuffer);
                    Game.GraphicsDevice.Indices = meshPart.IndexBuffer;
                    Game.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, meshPart.VertexOffset, 0, meshPart.NumVertices, meshPart.StartIndex, meshPart.PrimitiveCount);
                }
            }

        }
        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime)
        {
            Draw(gameTime, Effect);
            DrawBox(bounds);
        }
    }
}
